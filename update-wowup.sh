#!/usr/bin/env bash

# Depends on curl, jq
# Include the flag --beta (or -b) to install the absolute latest version, even if its a beta release

WOW_UP_REPOSITORY='https://api.github.com/repos/wowup/wowup'
INSTALL_DIRECTORY='/opt/wowup'
SYSTEM_LINK_LOCATION='/usr/local/bin/wowup'

INCLUDE_BETA=false
if [ "${1}" = "-b" ] || [ "${1}" = "--beta" ]; then
  INCLUDE_BETA=true
fi

if $INCLUDE_BETA; then
  LATEST_UPDATE_RAW=$(curl -sH "Accept: application/vnd.github.v3+json" "${WOW_UP_REPOSITORY}/releases" | jq '.[0]')
  CURRENT_INSTALLED_VERSION=$(command ls ${INSTALL_DIRECTORY}/WowUp* | cut -d '-' -f 2 | sort -t. -k 1,1nr -k 2,2nr -k 3,3nr | head -n 1 | sed -e "s/\.AppImage//")
else
  LATEST_UPDATE_RAW=$(curl -sH "Accept: application/vnd.github.v3+json" "${WOW_UP_REPOSITORY}/releases/latest")
  CURRENT_INSTALLED_VERSION=$(command ls ${INSTALL_DIRECTORY}/WowUp* | grep -v 'beta' | cut -d '-' -f 2 | sort -t. -k 1,1nr -k 2,2nr -k 3,3nr | head -n 1 | sed -e "s/\.AppImage//")
fi

LATEST_UPDATE_VERSION=$(echo "${LATEST_UPDATE_RAW}" | jq -r '.tag_name')
LATEST_UPDATE_ASSET=$(echo "${LATEST_UPDATE_RAW}" | jq -r '.assets[] | select(.name | contains("AppImage"))')
LATEST_UPDATE_URL=$(echo "${LATEST_UPDATE_ASSET}" | jq -r '.url')
LATEST_UPDATE_NAME=$(echo "${LATEST_UPDATE_ASSET}" | jq -r '.name')
LATEST_UPDATE_FULL_PATH="${INSTALL_DIRECTORY}/${LATEST_UPDATE_NAME}"

function download_wowup() {
  local -r download_url="${1}"
  local -r install_full_path="${2}"
  sudo curl -LH "Accept: application/octet-stream" -o "${install_full_path}" "${download_url}"
}

function configure_wowup() {
  local -r install_full_path="${1}"
  sudo chmod +x "${install_full_path}"
  sudo rm -f "${SYSTEM_LINK_LOCATION}"
  sudo ln -s "${install_full_path}" "${SYSTEM_LINK_LOCATION}"
}

echo "Installed version is v${CURRENT_INSTALLED_VERSION}."

echo "Latest version is ${LATEST_UPDATE_VERSION}."
if [[ -f "${LATEST_UPDATE_FULL_PATH}" ]]; then
  echo "Latest version is already installed. Exiting..."
  exit 0
else
  echo "Installing latest version."
fi

echo "Downloading version ${LATEST_UPDATE_VERSION}."
download_wowup "${LATEST_UPDATE_URL}" "${LATEST_UPDATE_FULL_PATH}"
echo "Configuring WowUp."
configure_wowup "${LATEST_UPDATE_FULL_PATH}"
