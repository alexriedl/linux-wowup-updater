# Overview
Simple bash script used to update and install latest wowup appimage. Simply run this script when there is a new version available (or if you think there might be a new version available) and this script will make sure the latest version is installed

Depends on
* curl
* jq

# Example Usage

Basic usage
`./update-wowup.sh`

Include beta versions
`./update-wowup.sh --beta`
